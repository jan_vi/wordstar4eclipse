This project emulates the most used key sequences of Wordstar from the 
Wordstar N-Mode (Non-document mode) for use in programmers Eclipse editor.
To activate, start your Eclipse and select Window-Preferences-General-Keys.
Make sure, the Scheme Pulldown with default (not Default) is selected. 
Close Window by Apply&Close. Then use File-Import-General-Preference-Next 
und browse for the epf preference file provided by this project. Tick
Keys-Preferences und select Finish. Eclipse will start now again 
and shows Wordstar behaviour for future usage.

Enjoy the most used Wordstar Keyboard sequences, shift all the moves to 
your muscle memory and unleash your creativity.

The scroll functionality using cntl-W and cntl-Z is changed for 
German keyboard layout in ws_gr version. For US style  keyboards with same
Z & Y key position use the ws_us version.

All <cntl>-Key sequences are implemented double. For example, moving the Cursor
to begin of line is cntl-Q + cntl-S as well as cntl-q+s.

By default, Eclipse offers help menu similar to Wordstar. To activate, 
use Window-Preferences-General-Keys and tick the
"ShowKeyBindingWhenCommandIsInvoked" "through keyboard". If you hesitate  
after cntl-q a Quick menue for cntl-q appears what shows 
all possibilities for the second character of the cntl-q sequence.

cntl-I is not implemented intentionally as todays keyboards have tab key
cntl-N is not implemented intentionally as todays keyboards have cr key


Todo: cntl-K Block Menue, column block cntl-KN already working
Todo: The cnlt-O Onscreen menue is mainly intended for document mode
Therefore the sequences can be assigned to usefull programmer features
similar to the joes Editor is doing for the jstar version




 



